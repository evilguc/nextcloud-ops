# nextcloud-ops
* set `--git-url` to correct value in `fluxcd/patch.yaml`
* put kubernetes config to `.cli/home/user/.kube/config`
* enter cli `make cli`
    - install `flux` into cluster `kubectl apply -k fluxcd/`
    - get flux ssh key `fluxctl identity --k8s-fwd-ns flux`
* put key into deployments key

## DigitalOcean notes
* for ingress to work properly it is required to setup domain - make sure you have an A record pointing to created load balancer
